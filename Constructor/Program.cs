﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructor
{
    class BaseClass
    {
        public BaseClass()
        {
            Console.WriteLine("BC");
        }
    }

    class DerivedClass1 : BaseClass
    {
        public DerivedClass1()
        {
            Console.WriteLine("DC1");
        }
    }
    class DerivedClass2 : DerivedClass1
    {
        public DerivedClass2()
        {
            Console.WriteLine("DC2");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            BaseClass item1 = new BaseClass();
            BaseClass item2 = new DerivedClass1();
            BaseClass item3 = new DerivedClass2();
            object item4 = new DerivedClass1();
            object item5 = new DerivedClass2();
            Console.ReadLine();

        }
    }
}
